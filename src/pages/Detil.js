import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

export default function Detil() {
  let productId = useParams();
  const [datas, setData] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    async function getUser() {
      try {
        const response = await axios.get(
          "http://localhost:3002/mhs/" + productId.id
        );
        if (response.status === 200) {
          setData(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    getUser();
  }, [productId]);

  const handleDeleted = async (id) => {
    try {
      if (window.confirm("Yakin ?")) {
          axios.delete("http://localhost:3002/mhs/" + id);
          navigate("/");
      }
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className="my-3">
      <ul className="list-group list-group-flush">
        <li className="list-group-item">Nama : {datas.name}</li>
        <li className="list-group-item">Npm : {datas.npm}</li>
        <li className="list-group-item">Prodi : {datas.prodi} </li>
      </ul>
      <div className="d-flex justify-content-end" >
        <button
          className="btn btn-info mr-2"
          onClick={() => navigate(`/user/update/${datas.id}`)}
        >
          Edit
        </button>
        <button
          className="btn btn-info"
          onClick={() => handleDeleted(datas.id)}
        >
          Hapus
        </button>
      </div>
    </div>
  );
}
