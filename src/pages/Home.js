import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
const Home = () => {
  const navigate = useNavigate();
  const [data, setData] = useState([]);
  
 
  async function getUser() {
    try {
      const response = await axios.get("http://localhost:3002/mhs");
       setData(response.data);
         setData(response.data.mhs)
        console.log("response data :", response.data.mhs);
      // console.log("response:", response);
    } catch (error) {
      console.error(error);
    }
  }
  useEffect(() => {
   getUser()
  }, [])
  return (
    <div className="my-3">
      <div className="row my-3">
        <button className="btn btn-primary" onClick={() => navigate("/user/tambah")} >Tambahkan</button>
      </div>
      <table className="table table-borderless">
        <thead>
          <tr>
            <th scope="col">Nama</th>
            <th scope="col">Npm</th>
            <th scope="col">Prodi</th>
            <th scope="col">Detil</th>
          </tr>
        </thead>
        <tbody>
          {
            data.map((data)=> {
              return (
                <tr key={data.id} >
                  <td>{data.name}</td>
                  <td>{data.npm}</td>
                  <td>{data.prodi}</td>
                  <td>
                    <button
                      className="btn btn-info"
                      onClick={() => navigate(`/detil/${data.id}`)}
                    >
                      Detil
                    </button>
                  </td>
                </tr>
              );
            })
          }
        </tbody>
      </table>
    </div>
  );
};

export default Home;
