import axios from "axios";
import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router";
import { useForm } from "react-hook-form";

function UpdateData() {
    const {
    register,
    handleSubmit,
    formState: { errors },
    } = useForm();
  const navigate = useNavigate()
  const params = useParams();
  const [post, setPost] = useState({
    name: "M Firman Setiawan",
    npm: 120809,
    prodi: "informatiak",
  });
  

  const handleChange = (e, name) => {
    const value = e.target.value;
    setPost({ ...post, [name]: value });
  };

  useEffect(() => {
    const getData = async () => {
        try {
            const response = await axios.get(
              `http://localhost:3002/mhs/${params.id}`, 
              
            );
            setPost(response.data)

        } catch (err) {
            console.log(err);
        }
    }
    getData()
  }, [params])


  const onSubmit = async () => {
    try{
    const response = await axios.put(`http://localhost:3002/mhs/${params.id}`, post );
    navigate("/")
    console.log("response upate :" , response);
    } catch (err) {
        console.log(err);
    }
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <label>Nama</label>
          <input
            type="text"
            {...register("name", { required: true })}
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            onChange={(e) => handleChange(e, "name")}
            value={post.name}
          />
          {errors.name && (
            <small class="form-text  text-danger">Perlu masukan Nama</small>
          )}
        </div>
        <div className="form-group">
          <label>Npm</label>
          <input
            type="number"
            {...register("npm", { required: true })}
            className="form-control"
            id="exampleInputEmail1"
            onChange={(e) => handleChange(e, "npm")}
            value={post.npm}
          />
          {errors.npm && (
            <small class="form-text  text-danger">Perlu masukan Npm</small>
          )}
        </div>
        <div className="form-group">
          <label>Prodi</label>
          <input
            type="text"
            {...register("prodi", { required: true })}
            className="form-control"
            id="exampleInputEmail1 "
            onChange={(e) => handleChange(e, "prodi")}
            value={post.prodi}
          />
          {errors.prodi && (
            <small class="form-text  text-danger">Perlu masukan prodi</small>
          )}
        </div>

        <button
          type="submit"
          //   onClick={handleSubmit}
          className="btn btn-primary"
        >
          Submit
        </button>
      </form>
    </div>
  );
}

export default UpdateData;
