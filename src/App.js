import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Detil from "./pages/Detil";
import Layout from "./components/layout";
import TambahData from "./pages/TambahData";
import UpdateData from "./pages/Update";
import About from "./pages/About";
// import User from "../.."

function App() {
  return (
    <div className="App">
      <Layout>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/detil/:id" element={<Detil />} />
            <Route path="/user/tambah" element={<TambahData/>} />
            <Route path="/user/update/:id" element={<UpdateData />} />
            <Route path="/about" element={<About />} />
          </Routes>
        </Router>
      </Layout>
    </div>
  );
}

export default App;
