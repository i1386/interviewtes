# @crud kalakreatif

### @BaseUri = http://localhost:3002


### Front End
npm start

### Back End
json-server --watch db.json --port 3002

### Get all data
GET {{baseUri}}/mhs 
Content-Type: application/json

### Add data
GET {{baseUri}}/mhs 
Content-Type: application/json

{
  "name": "string",
  "npm": 0,
  "prodi": string,
}
 



### Update data
PUT {{baseUri}}/patients/{{id}} HTTP/1.1
Content-Type: application/json

{
  {
  "name": "string",
  "npm": 0,
  "prodi": string,
}
}

### Delete data
DELETE {{baseUri}}/mhs/{{id}} 
Content-Type: application/json
